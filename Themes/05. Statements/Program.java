import java.util.Scanner;

class Program {
	public static void main(String[] args) {
			Scanner scanner = new Scanner(System.in);

			int number = scanner.nextInt();

			boolean isEven = number % 2 == 0;
			if (isEven) {
				System.out.println("EVEN");
			} else {
				System.out.println("ODD");
			}

			System.out.println("Please, enter numbers count:");
			int count = scanner.nextInt();

			System.out.println("Please, enter " + count + " numbers");

			int i = 0;

			int sum = 0;
			while (i < count) {
				int currentNumber = scanner.nextInt();
				sum = sum + currentNumber;
				// i = i + 1; i += 1
				i++;
			}

			System.out.println("Sum is " + sum);
	}	
}