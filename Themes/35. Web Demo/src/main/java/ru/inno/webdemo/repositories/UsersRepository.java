package ru.inno.webdemo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.webdemo.models.User;

import java.util.List;

/**
 * 06.12.2020
 * 35. Web Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface UsersRepository extends JpaRepository<User, Long> {
    List<User> findAllByFirstNameIsLike(String firstName);
}
