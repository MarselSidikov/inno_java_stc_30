import java.util.Scanner;

class Program {

	public static void swapInArray(int a[], int i, int j) {
		int temp = a[i];
		a[i] = a[j];
		a[j] = temp;
	}

	public static void swap(int x, int y) {
		int temp = x;
		x = y;
		y = temp;
		System.out.println(x + " " + y);
	}

	public static void printInRange(int a, int b) {
		for (int i = a; i <= b; i++) {
			System.out.print(i + " ");
		}
		System.out.println();
	}

	public static void main(String[] args) {
		printInRange(1, 10);
		printInRange(5, 11);

		Scanner scanner = new Scanner(System.in);

		int x = scanner.nextInt();
		int y = scanner.nextInt();

		printInRange(x, y);

		int m = 10;
		int n = 15;

		swap(m, n);
		System.out.println(m + " " + n);

		System.out.println("Work with arrays: ");

		int[] array = {1, 2, 3, 4, 5, 6, 7};
		swapInArray(array, 2, 6);
		System.out.println(array[2] + " " + array[6]);
	}
}