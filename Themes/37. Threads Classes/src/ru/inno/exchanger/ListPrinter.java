package ru.inno.exchanger;

import java.util.List;
import java.util.concurrent.Exchanger;

/**
 * 11.12.2020
 * 37. Threads Classes
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class ListPrinter implements Runnable {

    private final Exchanger<List<Integer>> exchanger;

    public ListPrinter(Exchanger<List<Integer>> exchanger) {
        this.exchanger = exchanger;
    }

    @Override
    public void run() {
        while (true) {
            List<Integer> currentList = null;
            try {
                currentList = exchanger.exchange(null);
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
            System.out.println(currentList);
        }
    }
}
