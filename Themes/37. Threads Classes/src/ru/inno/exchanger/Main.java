package ru.inno.exchanger;

import java.util.List;
import java.util.concurrent.Exchanger;

/**
 * 11.12.2020
 * 37. Threads Classes
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Exchanger<List<Integer>> exchanger = new Exchanger<>();
        new Thread(new ListPrinter(exchanger)).start();
        new Thread(new PrimesThread(0, 2100000000, 20000, exchanger)).start();
    }
}
