package ru.inno.count_down_latch;

import java.util.Deque;
import java.util.LinkedList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 07.12.2020
 * 36. ThreadPool
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class ThreadPoolImpl implements TaskExecutorService {

    private final Deque<Runnable> tasks;

    private final WorkerThread workerThreads[];
    // счетчик, который будет считать - сколько потоков уже готово к работе?
    private final CountDownLatch readyCountDownLatch;
    // счетчик, который будет считать - сколько задач выполнено?
    private final CountDownLatch completeCountDownLatch;

    private static Lock lock = new ReentrantLock();

    public ThreadPoolImpl(int threadsCount, CountDownLatch completeCountDownLatch) {
        this.tasks = new LinkedList<>();
        this.workerThreads = new WorkerThread[threadsCount];
        this.completeCountDownLatch = completeCountDownLatch;
        // счетчик будет отсчитывать момент, пока ВСЕ потоки не запустятся
        this.readyCountDownLatch = new CountDownLatch(threadsCount);
        for (int i = 0; i < workerThreads.length; i++) {
            this.workerThreads[i] = new WorkerThread("poolWorker-" + i);
            // запускаем каждый поток
            this.workerThreads[i].start();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    @Override
    public void submit(Runnable task) {
        // если кладем новую задачу в очередь, то мы не хотим, чтобы в этот момент ее меняли
        synchronized (tasks) {
            tasks.add(task);
            // оповестим поток, который был в ожидании о том, что есть новая задача
            tasks.notify();
        }
    }

    private class WorkerThread extends Thread {

        public WorkerThread(String threadName) {
            super(threadName);
        }
        @Override
        public void run() {
            // как только новый поток пула запустился
            // отсчитываем в минус счетчик
            lock.lock();
            readyCountDownLatch.countDown(); // j--

            System.out.println(Thread.currentThread().getName() + " Готово потоков - " + (workerThreads.length - readyCountDownLatch.getCount()));
            // пока все потоки не готовы, этот поток должен подождать
            try {
                System.out.println(Thread.currentThread().getName() + " ушли в ожидание");
                lock.unlock();
                readyCountDownLatch.await(); // ждет, пока j не будет равен нулю
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
            System.out.println(Thread.currentThread().getName() + " вышли из ожидания");
            Runnable task;
            while (true) {
                // заблокировать очередь задач, чтобы пока мы ее смотрим, никто больше не мог туда ничего положить
                synchronized (tasks) {
                    while (tasks.isEmpty()) {
                        // уходим в ожидание, пока в очередь ничего не положат
                        try {
                            tasks.wait();
                        } catch (InterruptedException e) {
                            throw new IllegalStateException(e);
                        }
                    }
                    task = tasks.poll();
                }
                System.out.println(Thread.currentThread().getName() + " начата загрузка файла");
                task.run();
                // уменьшили счетчик количества задач
                completeCountDownLatch.countDown(); // i--
                System.out.println(Thread.currentThread().getName() + " загрузка окончена");
            }
        }
    }
}
