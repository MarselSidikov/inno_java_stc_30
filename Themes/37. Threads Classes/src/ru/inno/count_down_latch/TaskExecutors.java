package ru.inno.count_down_latch;

import java.util.concurrent.CountDownLatch;

/**
 * 07.12.2020
 * 36. ThreadPool
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class TaskExecutors {

    public static TaskExecutorService threadPool(int threadsCount, CountDownLatch completeCountDownLatch) {
        return new ThreadPoolImpl(threadsCount, completeCountDownLatch);
    }
}
