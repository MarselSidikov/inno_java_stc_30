package ru.inno;

/**
 * 28.10.2020
 * 19. Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface List extends Collection {
    int get(int index);
    int indexOf(int element);
    void removeByIndex(int index);
    void insert(int element, int index);
}
