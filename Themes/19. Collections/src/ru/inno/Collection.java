package ru.inno;

/**
 * 28.10.2020
 * 19. Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// не гарантирует порядок элементов
public interface Collection extends Iterable {
    void add(int element);
    boolean contains(int element);
    int size();
    void removeFirst(int element);
}
