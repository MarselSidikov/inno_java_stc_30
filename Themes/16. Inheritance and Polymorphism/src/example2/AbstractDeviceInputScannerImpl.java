package example2;

import java.util.Scanner;

/**
 * 15.10.2020
 * 16. Inheritance & Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public abstract class AbstractDeviceInputScannerImpl {
    protected Scanner scanner = new Scanner(System.in);
}
