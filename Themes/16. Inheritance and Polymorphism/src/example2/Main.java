package example2;

/**
 * 15.10.2020
 * 16. Inheritance & Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        DeviceOutputPrefixImpl deviceOutput = new DeviceOutputPrefixImpl("MESSAGE: ");
//        deviceOutput.print("Hello!");

        DeviceInputPrefixImpl deviceInput = new DeviceInputPrefixImpl("FROM CONSOLE: ");
//        System.out.println(deviceInput.read());

        IOService service = new IOService(deviceInput, deviceOutput);
        service.print("Hello!");
        System.out.println(service.read());

        DeviceOutputTimeImpl deviceOutputTime = new DeviceOutputTimeImpl();
        deviceOutputTime.print("Bye!");
    }
}
