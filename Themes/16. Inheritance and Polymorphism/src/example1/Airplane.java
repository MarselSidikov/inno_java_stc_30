package example1;

/**
 * 15.10.2020
 * 16. Inheritance & Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Airplane extends Transport {

    private double length;

    public Airplane(double fuelAmount, double fuelConsumption, double length) {
        // вызов конструктора предка
        super(fuelAmount, fuelConsumption);
        this.length = length;
    }

    // переопределение метода - мы пишем новую реализацию метода
    // который уже есть в предке
    // этот метод должен полностью повторять сигнатуру метода предка
    @Override
    public void go(int km) {
        System.out.println("Я самолет и я лечу!");
        this.fuelAmount -= (km * fuelConsumption) / 100;
        this.fuelConsumption =- 0.1;
    }

    @Override
    public void stop() {
        System.out.println("Приземляюсь!");
    }

    public double getLength() {
        return length;
    }
}
