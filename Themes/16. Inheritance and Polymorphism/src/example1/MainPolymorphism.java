package example1;

/**
 * 15.10.2020
 * 16. Inheritance & Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainPolymorphism {
    public static void main(String[] args) {
        // Мы видим объект через призму типа ссылочной переменной
        Transport t1 = new Airplane(350, 4, 30);
        Transport t2 = new MilitaryAirplane(100, 10, 30, 10);
        Transport t3 = new Tank(100, 5, 10, 0.5);

        // полиморфизм - работаем с объектами разных типов так,
        // будто они принадлежат одному типу
        Transport transports[] = {t1, t2, t3};

        for (int i = 0; i < transports.length; i++) {
            transports[i].stop();
        }

        ((Tank)(t3)).fire();
        ((Tank)(t1)).fire();
    }
}
