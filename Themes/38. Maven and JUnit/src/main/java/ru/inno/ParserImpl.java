package ru.inno;

/**
 * 13.12.2020
 * 38. Maven and JUnit
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class ParserImpl implements Parser {
    public int parse(String string) {

        if (string == null || string.equals("")) {
            throw new IllegalArgumentException();
        }

        int result = 0;
        char number[] = string.toCharArray();
        int mult = 1;
        int sign = 1;

        int finish = 0;
        if (number[0] == '-') {
            sign = -1;
            finish = 1;
        } else if (number[0] == '+') {
            finish = 1;
        }

        for (int i = number.length -1; i >= finish; i--) {
            if (!Character.isDigit(number[i])) {
                throw new IllegalArgumentException();
            }
            // '9'
            // -3542 = 3 * 10^3 + 5 * 10^2 + 4 * 10^1 + 2 * 10^0
            result = result + (number[i] - '0') * mult;
            mult *= 10;
        }

        return result * sign;
    }
}
