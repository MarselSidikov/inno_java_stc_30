package ru.inno.models;

import lombok.*;

import java.util.List;

/**
 * 17.12.2020
 * 40. JDBC
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Data
@AllArgsConstructor
@Builder
public class Account {
    private Long id;
    private String firstName;
    private String lastName;
    private Integer age;
    private Integer experience;
    private List<Car> cars;
}
