package ru.inno;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.inno.models.Account;
import ru.inno.repositories.AccountsRepository;
import ru.inno.repositories.AccountsRepositoryJdbcImpl;

import java.util.Optional;
import java.util.Scanner;

/**
 * 17.12.2020
 * 40. JDBC
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl("jdbc:postgresql://localhost:5432/stc_db_2020");
        hikariConfig.setDriverClassName("org.postgresql.Driver");
        hikariConfig.setUsername("postgres");
        hikariConfig.setPassword("qwerty007");
        hikariConfig.setMaximumPoolSize(20);

        HikariDataSource dataSource = new HikariDataSource(hikariConfig);

        AccountsRepository accountsRepository = new AccountsRepositoryJdbcImpl(dataSource);

//        accountsRepository.save(account);
//        System.out.println(account.getId());
//        System.out.println(accountsRepository.findAll());
//        Optional<Account> kamil = accountsRepository.findById(20L);
//        if (kamil.isPresent()) {
//            System.out.println(kamil.get());
//        } else {
//            System.out.println("Нет такого пользователя");
//        }

        System.out.println(accountsRepository.findAll(3, 2));
    }
}
