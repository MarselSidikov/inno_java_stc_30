package ru.inno.repositories;

import ru.inno.models.Account;

import java.util.List;
import java.util.Optional;

/**
 * 17.12.2020
 * 40. JDBC
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface AccountsRepository extends CrudRepository<Account, Long> {
    List<Account> findAllByAge(Integer age);
}
