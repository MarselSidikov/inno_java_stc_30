package ru.inno.lambdas;

/**
 * 19.10.2020
 * 17. Anon Classes, Functional Interfaces, Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class ClassicMain {
    public static void main(String[] args) {
        Extractor extractor = new SimpleExtractorFrom0To10Impl();
        String line = extractor.extract("Привет, как дела, что нового? Зачем пришел?");
        System.out.println(line);
    }
}
