package ru.inno.lambdas;

/**
 * 19.10.2020
 * 17. Anon Classes, Functional Interfaces, Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class SimpleExtractorFrom0To10Impl implements Extractor {

    @Override
    public String extract(String text) {
        return text.substring(0, 10);
    }
}
