package ru.inno.lambdas;

/**
 * 19.10.2020
 * 17. Anon Classes, Functional Interfaces, Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class AnonMain {
    public static void main(String[] args) {
        Extractor extractor = new Extractor() {
            @Override
            public String extract(String text) {
                return "Порезали: " + text.substring(5, 10);
            }
        };

        String line = extractor.extract("Привет, как дела, ну пока, я пошел");
        System.out.println(line);
    }
}
