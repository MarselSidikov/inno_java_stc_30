package ru.inno.functional;

/**
 * 19.10.2020
 * 17. Anon Classes, Functional Interfaces, Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface ConvertFunction {
    String convert(String line, String format);
}
