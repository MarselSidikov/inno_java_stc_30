package ru.inno.functional;

import java.util.Arrays;

/**
 * 19.10.2020
 * 17. Anon Classes, Functional Interfaces, Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        // (параметры) -> { тело функции }
//        ConvertFunction function = (line, format) -> {
//            char lineAsArray[] = line.toCharArray();
//            String result = "";
//            for (int i = 0; i < line.length(); i++) {
//                if (format.equals("%d -> %s")) {
//                    if (Character.isDigit(lineAsArray[i])) {
//                        String newElementInString;
//
//                        switch (lineAsArray[i]) {
//                            case '1': {
//                                newElementInString = "one";
//                            }
//                            break;
//                            case '2': {
//                                newElementInString = "two";
//                            }
//                            break;
//                            case '3': {
//                                newElementInString = "three";
//                            }
//                            break;
//                            default: {
//                                newElementInString = "";
//                            }
//                        }
//                        result += newElementInString;
//                    }
//                }
//            }
//            return result;
//        };

        String lines[] = {"He1llo", "123ll", "B2ye"};

        TextProcessor textProcessor = new TextProcessor(lines, "%d -> %s");


        String resultLines[] = textProcessor.process((line, format) -> {
            char lineAsArray[] = line.toCharArray();
            String result = "";
            for (int i = 0; i < line.length(); i++) {
                if (format.equals("%d -> %s")) {
                    if (Character.isDigit(lineAsArray[i])) {
                        String newElementInString;

                        switch (lineAsArray[i]) {
                            case '1': {
                                newElementInString = "one";
                            }
                            break;
                            case '2': {
                                newElementInString = "two";
                            }
                            break;
                            case '3': {
                                newElementInString = "three";
                            }
                            break;
                            default: {
                                newElementInString = "";
                            }
                        }
                        result += newElementInString;
                    }
                }
            }
            return result;
        });
        System.out.println(Arrays.toString(resultLines));
    }
}
