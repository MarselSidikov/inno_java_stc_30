package ru.inno.anonims;

/**
 * 19.10.2020
 * 17. Anon Classes, Functional Interfaces, Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public abstract class AbstractWeapon {
    private int bulletsCount;

    public AbstractWeapon(int bulletsCount) {
        this.bulletsCount = bulletsCount;
    }

    public void reload(int bulletsCount) {
        this.bulletsCount = bulletsCount;
    }

    public abstract void fire();
}
