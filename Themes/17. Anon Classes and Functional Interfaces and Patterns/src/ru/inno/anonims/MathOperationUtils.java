package ru.inno.anonims;

/**
 * 19.10.2020
 * 17. Anon Classes, Functional Interfaces, Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface MathOperationUtils {
    int sum(int a, int b);
    int div(int a, int b);
}