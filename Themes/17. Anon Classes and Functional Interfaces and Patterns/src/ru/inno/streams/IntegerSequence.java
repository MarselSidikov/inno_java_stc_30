package ru.inno.streams;

/**
 * 19.10.2020
 * 17. Anon Classes, Functional Interfaces, Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */

// класс, который описывает какую-то последовательность чисел
// с которой можно что-то делать
public class IntegerSequence {
    // массив - последовательность чисел
    private int sequence[];
    // количествто чисел в последовательности
    private int count = 0;

    // конструктор принимает на вход готовый массив в числами
    // и делает его копию для нас
    public IntegerSequence(int[] sequence) {
        // создаем новый массив, размер которого равен исходному
        this.sequence = new int[sequence.length];
        // поэлементно копируем элементы исходного массива в наш массив
        for (int i = 0; i < sequence.length; i++) {
            this.sequence[i] = sequence[i];
        }
        // количество элементов массива нашего равно количеству элементов
        // исходного массива
        this.count = sequence.length;
    }

    // убирает все элементы массива, которые не удовлетворяют
    // какому-либо условию предиката
    public IntegerSequence filter(Predicate predicate) {
        // создаем новый массив
        int filteredSequence[] = new int[sequence.length];
        // новое количество элементов
        int newCount = 0;
        // пробегаем по исходному массиву
        for (int i = 0; i < this.sequence.length; i++) {
            // проверяет, удовлетворяет ли элемент условию теста
            if (predicate.test(sequence[i])) { // if sequence[i] % 2 == 0
                // если да, кидаем его в новый массив
                filteredSequence[newCount] = sequence[i];
                newCount++;
            }
        }
        // ставим новый массив на место старого
        this.sequence = filteredSequence;
        this.count = newCount;
        // возвращаем самого себя
        return this;
    }

    // изменяет все элементы массива по какому-то правилу из функции
    public IntegerSequence map(Function function) {
        // пробегаем все элементы массива
        for (int i = 0; i < count; i++) {
            // заменяем исходный элемент
            // значением, которое получилось после применения
            // метода apply над этим значением
            this.sequence[i] = function.apply(sequence[i]);
        }
        // возвращаем самого себя
        return this;
    }

    // применяет какое-либо действие из consumer к каждому элементу массива
    public void forEach(Consumer consumer) {
        // пробегаем все значения массива
        for (int i = 0; i < count; i++) {
            // применяем apply consumer-а
            consumer.apply(sequence[i]);
        }
    }
}
