package ru.inno.streams;


/**
 * 19.10.2020
 * 17. Anon Classes, Functional Interfaces, Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        int sourceArray[] = {12, 543, 12, 567, 3321,
                787654, 333, 22,
                1, 78, 9, 65, 43,
                2, 4, 56, 78, 532};
        IntegerSequence sequence = new IntegerSequence(sourceArray);

//        sequence.filter(value -> value % 2 == 0);
//        sequence.map(value -> value % 10);
//        sequence.forEach(value -> System.err.print(value + ", "));
        sequence.filter(value -> value % 2 == 0)
                .map(value -> value % 10)
                .map(value -> value * 10)
                .filter(value -> value == 20)
                .forEach(value -> System.err.print(value + ", "));

//        Consumer consumer = new Consumer() {
//            @Override
//            public void apply(int value) {
//                System.out.println(value);
//            }
//        };

//        Consumer consumer = value -> System.out.println(value);

//        Predicate predicate = new Predicate() {
//            @Override
//            public boolean test(int value) {
//                if (value % 2 == 0) {
//                    return true;
//                } else {
//                    return false;
//                }
//            }
//        };

//        Predicate predicate = value -> {
//            if (value % 2 == 0) {
//                return true;
//            } else {
//                return false;
//            }
//        };

//        Predicate predicate = value -> {
//            return value % 2 == 0;
//        };

//        Predicate predicate = value -> value % 2 == 0;

//        Function function = new Function() {
//            @Override
//            public int apply(int value) {
//                int digitsSum = 0;
//                while (value != 0) {
//                    digitsSum += value % 10;
//                    value = value / 10;
//                }
//                return digitsSum;
//            }
//        };

//        Function function = value -> {
//            int digitsSum = 0;
//            while (value != 0) {
//                digitsSum += value % 10;
//                value = value / 10;
//            }
//            return digitsSum;
//        };
//
//        sequence.map(value -> {
//            int digitsSum = 0;
//            while (value != 0) {
//                digitsSum += value % 10;
//                value = value / 10;
//            }
//            return digitsSum;
//        });
//        sequence.filter(value -> value % 2 == 0);
//        sequence.forEach(value -> System.out.println(value));
    }
}
