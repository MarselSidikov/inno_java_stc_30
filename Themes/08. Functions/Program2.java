class Program2 {
	public static boolean isEven(int number) {
		// if (number % 2 == 0) {
		// 	return true;
		// } else {
		// 	return false;
		// }
		return number % 2 == 0;
	}

	public static int sumDigits(int number) {
		int x0 = number % 10;
		number = number / 10;
		int x1 = number % 10;
		number = number / 10;
		int x2 = number % 10;

		return x0 + x1 + x2;
	}

	public static void main(String[] args) {
		boolean a1 = isEven(7);
		boolean a2 = isEven(8);
		boolean a3 = isEven(9);
		boolean a4 = isEven(10);

		int b1 = sumDigits(333);
		int b2 = sumDigits(444);
		int b3 = sumDigits(555);

		// boolean a5 = sumDigits(666);
	}
}