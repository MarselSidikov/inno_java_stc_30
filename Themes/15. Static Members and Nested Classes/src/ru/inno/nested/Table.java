package ru.inno.nested;

/**
 * 12.10.2020
 * 15. Static Members and Nested Classes
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Table {
    // максимальное количество элементов таблицы
    private static final int MAX_TABLE_SIZE = 10;

    private TableEntry table[];

    // количество элементов таблицы
    private int count = 0;

    public Table() {
        this.table = new TableEntry[MAX_TABLE_SIZE];
    }

    public void put(String key, int value) {
        if (count < MAX_TABLE_SIZE) {
            TableEntry tableEntry = new TableEntry(key, value);
            table[count] = tableEntry;
            count++;
        } else {
            System.err.println("Таблица переполнена");
        }
    }

    public int get(String key) {
        for (int i = 0; i < count; i++) {
            TableEntry current = table[i];

            if (current.getKey().equals(key)) {
                return current.getValue();
            }
        }
        System.err.println("Ключ не найден");
        return -1;
    }

    public TableEntry[] getTable() {
        return table;
    }

    public int getCount() {
        return count;
    }

    // вложенный класс (вложенный статический) - ассоциирован с обрамляющим классом
    private static class TableEntry {
        String key;
        int value;

        TableEntry(String key, int value) {
            this.key = key;
            this.value = value;
        }

        String getKey() {
            return key;
        }

        int getValue() {
            return value;
        }
    }

    // внутренний класс (inner) - ассоциирован с объектом обрамляющего класса
    public class TablePrinter {

        private String tableHeaderFormat;

        private String tableRowFormat;

        public TablePrinter(int keyWeight, int valueWeight) {
            this.tableHeaderFormat = "%" + keyWeight + "s|%" + valueWeight + "s\n";
            this.tableRowFormat = "%" + keyWeight + "s|%" + valueWeight + "d\n";
        }

        public void printTable(String keyName, String valueName) {
            System.out.printf(tableHeaderFormat, keyName, valueName);
            System.out.println("---------------------");
            for (int i = 0; i < count; i++) {
                System.out.printf(tableRowFormat, table[i].getKey(), table[i].getValue());
            }
        }
    }
}
