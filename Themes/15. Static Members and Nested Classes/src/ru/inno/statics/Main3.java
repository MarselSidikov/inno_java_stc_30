package ru.inno.statics;

import java.util.Arrays;

import static java.lang.Math.*;
/**
 * 12.10.2020
 * 15. Static Members and Nested Classes
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main3 {
    public static void main(String[] args) {
        double i = sqrt(155);
        i = PI;
        String arrayAsString = Arrays.toString(new int[]{4, 5, 10, 15});
    }
}
