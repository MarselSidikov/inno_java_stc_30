package ru.inno.statics;

/**
 * 12.10.2020
 * 15. Static Members and Nested Classes
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main2 {
    public static void main(String[] args) {
        SomeClass anObject1 = new SomeClass();
        anObject1.b = 5;

        SomeClass anObject2 = new SomeClass();
        anObject2.b = 10;

        SomeClass anObject3 = new SomeClass();
        anObject3.b = 15;

//        SomeClass.a = 35;

        anObject2.someMethod();
        System.out.println(anObject1.b + " " + anObject1.a);
        System.out.println(anObject2.b + " " + anObject2.a);
        System.out.println(anObject3.b + " " + anObject3.a);

        SomeClass.someStaticMethod();
        System.out.println(SomeClass.a);
    }
}
