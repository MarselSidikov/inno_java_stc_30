package ru.inno.statics;

import java.util.Random;

/**
 * 12.10.2020
 * 15. Static Members and Nested Classes
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class SomeClass {

    public static final int MAX_AGE = 100;

    int b;

    // статическое поле - глобальная переменная
    static int a = 155; // нет смысла в инициализации
    static int c;
    // статический инициализатор (статический конструктор)
    static {
        Random random = new Random();
        a = random.nextInt(100);
        c = random.nextInt(1000);
    }

    public SomeClass() {

    }

    public SomeClass(int x) {
        a = x;
    }

    public void someMethod() {
        this.b = 777;
//        this.a = 888;
        a = 888;
    }

    public void someMethod(int a) {
//        this.a = a;
        SomeClass.a = a;
    }

    public static void someStaticMethod() {
        // this.b = 5; // нельзя - потому что метод статический и не предполагает
        // наличия объекта
        a = 500;
    }

    public static void anotherStaticMethod() {
        someStaticMethod();
        // someMethod(5);
    }
}
