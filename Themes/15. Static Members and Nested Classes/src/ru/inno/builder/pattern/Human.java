package ru.inno.builder.pattern;

/**
 * 12.10.2020
 * 15. Static Members and Nested Classes
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Human {
    private int age; // DEFAULT - 0
    private String firstName; // DEFAULT - ""
    private String lastName; // DEFAULT - ""
    private boolean isFinishedCourse; // false

    public Human(int age, String firstName, String lastName, boolean isFinishedCourse) {
        this.age = age;
        this.firstName = firstName;
        this.lastName = lastName;
        this.isFinishedCourse = isFinishedCourse;
    }

    public int getAge() {
        return age;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public boolean isFinishedCourse() {
        return isFinishedCourse;
    }
}
