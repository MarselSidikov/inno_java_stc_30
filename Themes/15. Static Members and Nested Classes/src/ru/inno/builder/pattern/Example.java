package ru.inno.builder.pattern;

/**
 * 12.10.2020
 * 15. Static Members and Nested Classes
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Example {
    private String text = "";

    public Example append(String text) {
        this.text += text;
        return this;
    }

    public String getText() {
        return text;
    }
}
