package ru.inno.builder.pattern;

/**
 * 12.10.2020
 * 15. Static Members and Nested Classes
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
//        Human marsel = new Human("Марсель", "Сидиков");
//        Human marsel1 = new Human(true);
//        Human marsel2 = new Human(26, true);
//        Human marsel3 = new Human(26, "Сидиков");
//
//        Human human = Human.builder()
//                .firstName("Марсель")
////                .lastName("Сидиков")
//                .age(26)
////                .isFinishedCourse(true)
//                .build();

        Example example = new Example();
        example.append("Привет")
                .append("Как дела?")
                .append("Что нового");

        System.out.println(example.getText());
    }
}
