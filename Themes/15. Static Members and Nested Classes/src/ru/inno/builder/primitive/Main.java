package ru.inno.builder.primitive;

/**
 * 12.10.2020
 * 15. Static Members and Nested Classes
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Box box = Box.create(10, 5);
    }
}
