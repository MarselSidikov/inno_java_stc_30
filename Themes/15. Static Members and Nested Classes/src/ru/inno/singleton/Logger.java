package ru.inno.singleton;

import java.time.LocalDateTime;

/**
 * 12.10.2020
 * 15. Static Members and Nested Classes
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Logger {

    private final static Logger instance;

    static {
        instance = new Logger();
    }

    private Logger() {

    }

    public static Logger newLogger() {
        return instance;
    }

    public void info(String message) {
        System.out.println(createMessage(message));
    }

    public void error(String message) {
        System.err.println(createMessage(message));
    }

    private String createMessage(String message) {
        return LocalDateTime.now().toString() + " : " + message;
    }
}
