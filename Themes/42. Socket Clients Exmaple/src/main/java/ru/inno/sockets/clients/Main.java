package ru.inno.sockets.clients;

import java.util.Scanner;

/**
 * 23.12.2020
 * 42. Socket Clients Exmaple
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        SocketClient client = new SocketClient("localhost", 7777);

        Scanner scanner = new Scanner(System.in);
        String message = scanner.nextLine();
        // все время читаем сообщения с клавиатуры и отправляем их на сервер
        while (!message.equals("EXIT")) {
            client.sendMessage(message);
            message = scanner.nextLine();
        }
    }
}
