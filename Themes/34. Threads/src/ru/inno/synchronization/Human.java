package ru.inno.synchronization;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 03.12.2020
 * 34. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Human extends Thread {
    private String name;
    private final CreditCard creditCard;
    private static final Lock lock = new ReentrantLock();

    public Human(String name, CreditCard creditCard) {
        this.name = name;
        this.creditCard = creditCard;
    }

//    @Override
//    public void run() {
//        for (int i = 0; i < 1000; i++) {
//            // creditCard - мьютекс (одноместный семафор), если его занял какой-либо поток
//            // другой поток не может его использовать, и не может выполнять код, который заблокирован
//            // этим мьютексом
//            synchronized (creditCard) {
//                if (creditCard.getAmount() > 0) {
//                    System.out.println(name + " покупает");
//
//                    try {
//                        Thread.sleep(500);
//                    } catch (InterruptedException e) {
//                        throw new IllegalStateException(e);
//                    }
//
//                    if (creditCard.buy(10)) {
//                        System.out.println(name + " купил");
//                    } else {
//                        System.out.println(name + " говорит ээээээ...");
//                    }
//                }
//            }
//        }
//    }

    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            lock.lock();
            if (creditCard.getAmount() > 0) {
                System.out.println(name + " покупает");

                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    throw new IllegalStateException(e);
                }

                if (creditCard.buy(10)) {
                    System.out.println(name + " купил");
                } else {
                    System.out.println(name + " говорит ээээээ...");
                }
            }
            lock.unlock();
        }
    }
}
