package ru.inno.synchronization;

import java.util.Scanner;

/**
 * 03.12.2020
 * 34. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        // Поток -> Thread, Stream, IO Stream
        CreditCard creditCard = new CreditCard(1000);
        Human husband = new Human("Муж", creditCard);
        Human wife = new Human("Жена", creditCard);

        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
        husband.start();
        wife.start();
    }
}
