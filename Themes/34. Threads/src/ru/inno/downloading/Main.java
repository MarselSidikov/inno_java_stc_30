package ru.inno.downloading;

import ru.inno.service.ThreadService;

import java.io.*;
import java.net.URL;
import java.util.Scanner;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * 03.12.2020
 * 34. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {

    public static void saveFile(String fileName) throws Exception {
        URL url = new URL(fileName);
        InputStream in = new BufferedInputStream(url.openStream());
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte[] buf = new byte[1024];
        int n = 0;
        while (-1 != (n = in.read(buf))) {
            out.write(buf, 0, n);
        }
        out.close();
        in.close();
        byte[] response = out.toByteArray();

        String newFileName = UUID.randomUUID().toString() + ".png";
        FileOutputStream fos = new FileOutputStream("images\\" + newFileName);
        fos.write(response);
        fos.close();
        System.out.println("FILE SAVED - " + newFileName);
    }

    public static void main(String[] args) throws Exception {
        System.out.println(Runtime.getRuntime().availableProcessors());
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();

        File file = new File("links.txt");
        BufferedReader reader = new BufferedReader(new FileReader(file));

//        ThreadService threadService = new ThreadService();
//        String lineFromFile = reader.readLine();
//        while (lineFromFile != null) {
//            final String _file = lineFromFile;
//            threadService.submit(() -> {
//                try {
//                    saveFile(_file);
//                } catch (Exception e) {
//                    throw new IllegalStateException(e);
//                }
//            });
//            lineFromFile = reader.readLine();
//        }


        reader.lines().collect(Collectors.toList()).parallelStream().forEach(fileName -> {
            try {
                saveFile(fileName);
            } catch (Exception e) {
                throw new IllegalStateException(e);
            }
        });
    }
}
