package ru.inno.iterable;

public class Main {

    public static void main(String[] args) {
        Car car = new Car();

//        Iterator<PartOfCar> partOfCarIterator = car.iterator();
//
//        while (partOfCarIterator.hasNext()) {
//            System.out.println(partOfCarIterator.next().getName());
//        }

        for (PartOfCar partOfCar : car) {
            System.out.println(partOfCar.getName());
        }

    }
}
