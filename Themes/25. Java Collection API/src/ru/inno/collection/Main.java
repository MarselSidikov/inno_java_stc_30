package ru.inno.collection;

import java.util.*;

/**
 * 09.11.2020
 * 25. Java Collection API
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        // Collection<E>

        // int size();
        // boolean isEmpty();
        // boolean contains(Object o);
        // Object[] toArray();
        // <T> T[] toArray(T[] a);
        // boolean add(E e);
        // boolean remove(Object o);
        // boolean containsAll(Collection<?> c);
        // boolean addAll(Collection<? extends E> c);
        // boolean removeAll(Collection<?> c);
        // void clear();

        // List<E>

        // E get(int index);
        // E set(int index, E element);
        // void add(int index, E element);
        // E remove(int index);
        // int indexOf(Object o);
        // int lastIndexOf(Object o);
        // List<E> subList(int fromIndex, int toIndex);

        // Set<E>

        // все методы аналогичны Collection

        // Map<K, V>

        // int size();
        // boolean isEmpty();
        // boolean containsKey(Object key);
        // boolean containsValue(Object value);
        // V get(Object key);
        // V put(K key, V value);
        // V remove(Object key);
        // void clear();
        // Set<K> keySet();
        // Collection<V> values();
        // Set<Map.Entry<K, V>> entrySet();
        /*
         *  interface Entry<K,V> {
         *      K getKey();
         *      V getValue();
         *  }
         */
        Map<String, String> map = new HashMap<>();
        map.put("Марсель", "Сидиков");
        map.put("Нияз", "Гафуров");
        map.put("Неля", "Аюпова");
        map.put("Алмаз", "Анваров");

        Set<String> keys = map.keySet();

        for (String key : keys) {
            System.out.println(key);
        }

        Collection<String> values = map.values();
        for (String value : values) {
            System.out.println(value);
        }

        Set<Map.Entry<String, String>> entries = map.entrySet();

        for (Map.Entry<String, String> entry : entries) {
            System.out.println(entry.getValue() + " " + entry.getKey());
        }

        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("hello");
        // elementData[size++] = e;
        // elementData[count] = e;
        // count++;

        LinkedList<String> linkedList = new LinkedList<>();
        linkedList.add("Hello");
        linkedList.get(0);

        //
        // hashCode = -985016399(10) -> 11000101010010011101011110110001
        // shifted = 11000101010010011101011110110001 >>> 16 -> 00000000000000001100010101001001
        // hash = h ^ shifted = 11000101010010010001001011111000
        // h =       11000101010010011101011110110001
        // shifted = 00000000000000001100010101001001
        // ^       = 11000101010010010001001011111000
        String value = "Марсель";
        // (h = key.hashCode()) ^ (h >>> 16);
        int h = value.hashCode();
        int shifted = h >>> 16; // зачем???????????
        // x >>> n сдвиг вправо на 16 бит, а все предыдущие заменить нулями
        int hash = h ^ shifted;
        System.out.println(h + " " + shifted + " " + hash);

        // tab[i = (n - 1) & hash]
        // int index = entries.length - 1 & key.hashCode();

        // HashMap в методе put
        // 1. берет хеш-код
        // 2. делает сдвиг на 16
        // 3. делает xor(^) исходного кода и сдвига
        // 4. на основе результата xor получает индекс, куда нужно положить элемент

        Double d1 = 0.01;
        Double d3 = 0.03;
        Double d4 = 0.04;

        // 01111000001010100110111010011010
        System.out.println(d1.hashCode());
        // 01111000001110100110111010011010
        System.out.println(d3.hashCode());
        // 01111000000010100110111010011010
        System.out.println(d4.hashCode());

        // у Double плохо переопределен hashCode и значимые разряды в начале
        // а последние часто совпадают - первые 16 бит разные, вторые 16 бит одинаковые
        // это порождает коллизии
        // entries.length - 1 & key.hashCode() -> она смотрит только последние биты, если длина массива 16, то & будет
        // работать только с 16-ю последними битами.
        // когда делаем сдвиг >>> значимые биты переходят из левой части в правую
        // а если хеш-код был нормальный, и у него последние биты были значимыми, а первые - нет?
        // тогда вызывается xor и "востанавливает" исходный вариант

        // в целом - HashMap позволяет работать с разными хешкодами, и делает незначимые биты значимими

    }
}
