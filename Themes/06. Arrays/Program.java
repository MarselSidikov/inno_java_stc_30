// объявление массива, ввод и вывод

import java.util.Scanner;
import java.util.Arrays;
class Program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int size = scanner.nextInt();
		int a[] = new int[size];

		for (int i = 0; i < a.length; i++) {
			a[i] = scanner.nextInt();
		}
		
		System.out.println(Arrays.toString(a));

		for (int i = a.length - 1; i >= 0; i--) {
			System.out.print(a[i] + " ");
		}
	}
}