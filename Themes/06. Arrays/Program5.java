		/**
		123
		45
		675
		28
		33
		998
		128
		-1

		a[][] -> 
		{
		a[0]null
		a[1]null
		a[2]null
		a[3]123, 33
		a[4]null
		a[5]45, 675
		a[6]null
		a[7]null
		a[8]28, 998, 128
		a[9]null
		}
		**/
import java.util.Scanner;
import java.util.Arrays;

class Program5 {
	public static void main(String args[]) {
		Scanner scanner = new Scanner(System.in);

		int allNumbers[] = new int[20]; // массив для хранения всех чисел, которые ввел пользователь
		int lastDigitCounts[] = new int[10]; // массив, где мы храним, сколько чисел закончилось на i-ую цифру, то есть если lastDigitsCount[6] = 7, это означает, что 7 чисел закончилось на цифру 6
		int currentNumber; // текущее считанное число
		int count = 0; //сколько всего мы чисел считали

		int resultArray[][] = new int[10][];

		int currentCounts[] = new int[10]; // 0, 0, 0, 0, 0, 0, 0

		// считываем первое число
		currentNumber = scanner.nextInt();
		// пока считанное нами число не равно -1
		while (currentNumber != -1) {
			// currentNumber % 10 - последняя цифра числа
			// мы в массиве lastDigitCounts увеличиваем значение под индексом последней цифры на 1
			
			// lastDigitCount[6] = 0
			// считали число 136
			// 136 % 10 = 6
			// lastDigitCount[136 % 10]++ -> lastDigitCount[6] = 1
			// считали число 146
			// 146 % 10 = 6
			// lastDigitCount[146 % 10]++ -> lastDigitCount[6] = 2
			lastDigitCounts[currentNumber % 10]++;
			// кладем считанное число в массив на первую пустую позицию
			allNumbers[count] = currentNumber;
			count++;
			// считываем новое
			currentNumber = scanner.nextInt();
		}

		for (int i = 0; i < lastDigitCounts.length; i++) {
			System.out.println("Last digit is " + i + 
				" has " +  lastDigitCounts[i] + " numbers");
		}

		// теперь создаем ступенчатый массив, где все строки имеют разную длину
		for (int i = 0; i < lastDigitCounts.length; i++) {
			// если есть числа, которые кончаются на i-ую
			if (lastDigitCounts[i] > 0) {
				// создаем массив, размер которого равен количеству чисел, которые закончились на i
				resultArray[i] = new int[lastDigitCounts[i]];
			}
		}


		// выводим итоговый массив
		for (int i = 0; i < resultArray.length; i++) {
			// если значение строки массива не null
			if (resultArray[i] != null) {
				// выводим его элементы, в количестве resultArray[i].length
				for (int j = 0; j < resultArray[i].length; j++) {
					System.out.print(resultArray[i][j] + " ");
				}
				System.out.println();
			} else {
				// если ничего нет, просто печатаем строку, и java нам выдаст null
				System.out.println(resultArray[i]);
			}
		}

		// заполним массив реальными данными
		for (int i = 0; i < count; i++) {
			// нужно в resultArray[lastDigit] положить значение allNumbers[i]
			int lastDigit = allNumbers[i] % 10;
			System.out.println("INFO -> allNumbers[i] " + allNumbers[i]);
			System.out.println("INFO -> lastDigit " + lastDigit);
			System.out.println("INFO -> currentCounts[lastDigit] " + currentCounts[lastDigit]);
			System.out.println("INFO -> resultArray[lastDigit][currentCounts[lastDigit]]" + resultArray[lastDigit][currentCounts[lastDigit]]);
			// в lastDigit-строку кладем под номер currentCount[lastDigit] число из allNumbers
			resultArray[lastDigit][currentCounts[lastDigit]] = allNumbers[i];
			currentCounts[lastDigit]++;
		}

		// выводим итоговый массив
		for (int i = 0; i < resultArray.length; i++) {
			// если значение строки массива не null
			if (resultArray[i] != null) {
				// выводим его элементы, в количестве resultArray[i].length
				for (int j = 0; j < resultArray[i].length; j++) {
					System.out.print(resultArray[i][j] + " ");
				}
				System.out.println();
			} else {
				// если ничего нет, просто печатаем строку, и java нам выдаст null
				System.out.println(resultArray[i]);
			}
		}

		System.out.println(Arrays.toString(allNumbers));
	}
}