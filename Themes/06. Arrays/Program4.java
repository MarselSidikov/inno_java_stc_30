// объявление и вывод двумерного массива
class Program4 {
	public static void main(String[] args) {
		int array[][] = new int[3][4];

		int a1[] = array[0];
		int a2[] = array[1];
		int a3[] = array[2];

		a2[0] = 5;
		a2[1] = 4;
		a2[2] = 7;
		a2[3] = 8;

		for (int i = 0; i < a2.length; i++) {
			System.out.print(a2[i] + " ");
		}

		System.out.println();

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 4; j++) {
				System.out.print(array[i][j] + " ");
			}
			System.out.println();
		}
	}
}