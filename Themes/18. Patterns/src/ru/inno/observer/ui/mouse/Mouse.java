package ru.inno.observer.ui.mouse;

/**
 * 22.10.2020
 * 18. Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Mouse {

    private MouseClickReaction reaction;
    private MouseClickReactionOnlyIntensive onlyIntensiveReaction;

    public void onClick(MouseClickReaction clickReaction) {
        this.reaction = clickReaction;
    }

    void onClick(MouseClickReactionOnlyIntensive onlyIntensiveReaction) {
        this.onlyIntensiveReaction = onlyIntensiveReaction;
    }

    void clickLeftButton() {
        reaction.handle(new MouseClickArgs(MouseButton.LEFT), MouseButton.LEFT.getIntensive());
        onlyIntensiveReaction.handle(MouseButton.LEFT.getIntensive());
    }

    void clickRightButton() {
        reaction.handle(new MouseClickArgs(MouseButton.RIGHT), 50);
        onlyIntensiveReaction.handle(MouseButton.RIGHT.getIntensive());
    }


}
