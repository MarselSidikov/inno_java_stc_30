package ru.inno.observer.ui.enums;

/**
 * 22.10.2020
 * 18. Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        State state = State.DELETED;
        state = State.valueOf("ACTIVE");
        User user = new User("Марсель", "Сидиков", state);
        System.out.println(user.getState().ordinal());
        System.out.println(user.getState().name());
    }
}
