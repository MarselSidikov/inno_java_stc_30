package ru.inno.observer.real;

/**
 * 22.10.2020
 * 18. Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface TextProcessor {
    void process(String text);
    void addObserver(TextObserver observer);
    void notifyObservers(char character);
}
