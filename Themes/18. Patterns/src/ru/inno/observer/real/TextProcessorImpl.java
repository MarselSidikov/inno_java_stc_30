package ru.inno.observer.real;

/**
 * 22.10.2020
 * 18. Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class TextProcessorImpl implements TextProcessor {

    private TextObserver textObservers[];
    private int count;

    public TextProcessorImpl() {
        this.textObservers = new TextObserver[10];
        this.count = 0;
    }

    @Override
    public void process(String text) {
        char textAsCharArray[] = text.toCharArray();
        for (int i = 0; i < textAsCharArray.length; i++) {
            notifyObservers(textAsCharArray[i]);
        }
    }

    @Override
    public void addObserver(TextObserver observer) {
        this.textObservers[count] = observer;
        count++;
    }

    @Override
    public void notifyObservers(char character) {
        for (int i = 0; i < count; i++) {
            textObservers[i].onCharacter(character);
        }
    }
}
