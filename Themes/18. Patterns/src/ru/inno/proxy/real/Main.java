package ru.inno.proxy.real;

import ru.inno.observer.ui.enums.User;

/**
 * 22.10.2020
 * 18. Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        MailsService mailsService = new MailsService();
        UsersServiceProxy usersServiceProxy = new UsersServiceProxy(email -> mailsService.sendMessage(email));
//        UsersServiceProxy usersServiceProxy = new UsersServiceProxy(mailsService::sendMessage);
    }
}
