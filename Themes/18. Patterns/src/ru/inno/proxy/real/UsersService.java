package ru.inno.proxy.real;

/**
 * 22.10.2020
 * 18. Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class UsersService {

    public void signUp(String email, String password) {
        System.out.println("Регистрация прошла успешно!");
    }

    public void signIn(String email, String password) {
        System.out.println("Вход прошел успешно!");
    }

    public void resetPassword(String email) {
        System.out.println("Заявка на сброс пароля отправлена");
    }
}
