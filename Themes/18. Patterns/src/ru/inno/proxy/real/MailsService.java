package ru.inno.proxy.real;

/**
 * 22.10.2020
 * 18. Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MailsService {
    public void sendMessage(String email) {
        System.out.println("Направлено на email" + email);
    }
}
