package ru.inno.proxy.trivial;

/**
 * 22.10.2020
 * 18. Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Driver {

    private String name;

    public Driver(String name) {
        this.name = name;
    }

    public void go() {
        System.out.println(name + " поехал!");
    }
}
