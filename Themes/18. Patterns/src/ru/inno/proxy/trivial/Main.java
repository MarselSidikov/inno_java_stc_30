package ru.inno.proxy.trivial;

import ru.inno.proxy.trivial.Driver;
import ru.inno.proxy.trivial.DriverProxy;

/**
 * 22.10.2020
 * 18. Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {

    public static void sayGo(Driver driver) {
        driver.go();
    }

    public static void main(String[] args) {
        Driver driver1 = new Driver("Алмаз");
        DriverProxy driver = new DriverProxy("Марсель");

        driver.setBefore(() -> System.out.println("Марсель собрался поехать!"));
//        driver.setInstead(() -> System.out.println("А чем собрался ехать?"));
        driver.setAfter(() -> System.out.println("А не кукухой ли?"));

        sayGo(driver);
        sayGo(driver1);

    }
}
