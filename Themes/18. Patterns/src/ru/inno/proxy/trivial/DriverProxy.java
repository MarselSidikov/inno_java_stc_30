package ru.inno.proxy.trivial;

/**
 * 22.10.2020
 * 18. Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class DriverProxy extends Driver {

    private After after;
    private Before before;
    private Instead instead;

    public void setAfter(After after) {
        this.after = after;
    }

    public void setBefore(Before before) {
        this.before = before;
    }

    public void setInstead(Instead instead) {
        this.instead = instead;
    }

    public DriverProxy(String name) {
        super(name);
    }

    @Override
    public void go() {
        // если есть поведение до
        if (before != null) {
            // вызвать это поведение
            before.execute();
        }

        // если есть поведение вместо
        if (instead != null) {
            // вызвать это поведение
            instead.execute();
        } else {
            // если не было поведения вместо, значит вызывать то
            // которое было по умолчанию у предка
            super.go();
        }

        // если было поведение "после"
        if (after != null) {
            // вызываем поведение после
            after.execute();
        }
    }
}
