package ru.inno.strategy;

/**
 * 22.10.2020
 * 18. Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class SearchAlgorithmBinaryImpl implements SearchAlgorithm {

    private int sequence[];

    public SearchAlgorithmBinaryImpl(int[] sequence) {
        this.sequence = sequence;
    }

    @Override
    public boolean search(int element) {
        for (int i = 0; i < sequence.length; i++) {
            int min = sequence[i];
            int indexOfMin = i;

            for (int j = i; j < sequence.length; j++) {
                if (sequence[j] < min) {
                    min = sequence[j];
                    indexOfMin = j;
                }
            }

            sequence[indexOfMin] = sequence[i];
            sequence[i] = min;
        }

        int left = 0;
        int right = sequence.length - 1;
        int middle;

        while (left <= right) {
            middle = (right + left) / 2;

            if (sequence[middle] < element) {
                left = middle + 1;
            } else if (sequence[middle] > element) {
                right = middle - 1;
            } else {
                return true;
            }
        }
        return false;
    }
}
