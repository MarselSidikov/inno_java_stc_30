package ru.inno.result;

import ru.inno.Map;

/**
 * 07.11.2020
 * 24. Associative Arrays
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class HashMapImpl<K, V> implements Map<K, V> {
    private static final int DEFAULT_SIZE = 16;

    private MapEntry<K, V> entries[] = new MapEntry[DEFAULT_SIZE];

    private static class MapEntry<K, V> {
        K key;
        V value;
        MapEntry<K, V> next;

        public MapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }

    // TODO: предусмотреть ЗАМЕНУ ЗНАЧЕНИЯ
    // put("Марсель", 27);
    // put("Марсель", 28);
    // у меня сохранятся оба значения, вам нужно сделать, чтобы хранилось только одно
    @Override
    public void put(K key, V value) {
        MapEntry<K, V> newMapEntry = new MapEntry<>(key, value);
        int index = key.hashCode() & (entries.length - 1);
        if (entries[index] == null) {
            entries[index] = newMapEntry;
        } else {
            // если уже в этой ячейке (bucket) уже есть элемент
            // запоминяем его
            MapEntry<K, V> current = entries[index];
            // теперь дойдем до последнего элемента в этом списке
            while (current.next != null) {
                current = current.next;
            }
            // теперь мы на последнем элементе, просто добавим новый элемент в конец
            current.next = newMapEntry;
        }
    }

    // TODO: реализовать
    @Override
    public V get(K key) {
        return null;
    }
}
