package ru.inno.hashes;

/**
 * 07.11.2020
 * 24. Associative Arrays
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainStringHashCode {
    public static int stringHashCode(String s) {
        // 72 101 108 108 111 33

        char value[] = s.toCharArray();
        int h = 0;
        if (value.length > 0) {
            for (int i = 0; i < value.length; i++) {
                h = 31 * h + value[i];
            }
        }
        return h;
    }

    // 1. h1 = 31 * 0 + 72 = 72
    // 2. h2 = 31 * (31 * 0 + 72) + 101
    // 3. h3 = 31 * (31 * (31 * 0 + 72) + 101) + 108
    // 4. h4 = 31 * (31 * (31 * (31 * 0 + 72) + 101) + 108) + 108
    // 5. h5 = 31 * (31 * (31 * (31 * (31 * 0 + 72) + 101) + 108) + 108) + 111
    // 6. h6 = 31 * (31 * (31 * (31 * (31 * (31 * 0 + 72) + 101) + 108) + 108) + 111) + 33
    // 31 * (31 * (31 * (31 * (31 * (31 * 0 + 72) + 101) + 108) + 108) + 111) + 33
    // sum : code * 31^((size - 1) - i)
    // в данном хеш-коде учитывается код символа, позиция символа и число 31
    // почему 31? - это простое число, следовательно, если мы умножим какое-то число на него
    // то будет сложнее получить его разложение на множители 20 * 10 = 200

    public static void main(String[] args) {
        String hello = "Hello!"; // H - 72, e = 101, l = 108, l = 108, o = 111, ! = 33
        System.out.println(hello.hashCode());
        char[] array = hello.toCharArray();
        for (int i = 0; i < array.length; i++) {
            System.out.print((int)array[i] + " ");
        }
    }
}
