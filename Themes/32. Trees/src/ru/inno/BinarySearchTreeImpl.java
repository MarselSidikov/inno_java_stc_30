package ru.inno;

import java.util.Deque;
import java.util.LinkedList;

/**
 * 02.12.2020
 * 32. Trees
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class BinarySearchTreeImpl<T extends Comparable<T>> implements  BinarySearchTree<T> {

    static class Node<E> {
        E value;
        Node<E> left;
        Node<E> right;

        public Node(E value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value.toString();
        }
    }

    private Node<T> root;

    @Override
    public void insert(T value) {
        this.root = insert(this.root, value);
    }

    private Node<T> insert(Node<T> root, T value) {
        if (root == null) {
            root = new Node<>(value);
        }
        // значение меньше корня
        else if (value.compareTo(root.value) < 0) {
            // добавляем в левое поддерево
            root.left = insert(root.left, value);
        } else {
            root.right = insert(root.right, value);
        }

        return root;
    }

    @Override
    public void printDfs() {
        dfs(root);
    }

    @Override
    public void printDfsByStack() {
        Deque<Node<T>> stack = new LinkedList<>();
        stack.push(root);

        Node<T> current;

        while (!stack.isEmpty()) {
            current = stack.pop();
            System.out.println(current.value + " ");
            if (current.left != null) {
                stack.push(current.left);
            }
            if (current.right != null) {
                stack.push(current.right);
            }
        }
    }

    @Override
    public void printBfs() {
        Deque<Node<T>> queue = new LinkedList<>();
        queue.add(root);

        Node<T> current;

        while (!queue.isEmpty()) {
            current = queue.poll();
            System.out.println(current.value + " ");
            if (current.left != null) {
                queue.add(current.left);
            }
            if (current.right != null) {
                queue.add(current.right);
            }
        }
    }

    private void dfs(Node<T> root) {
        System.out.println("in root = " + root);
        if (root != null) {
            dfs(root.left);
            System.out.println(root.value + " ");
            dfs(root.right);
        }
        System.out.println("from root = " + root);
    }

    @Override
    public void remove(T value) {

    }

    @Override
    public boolean contains(T value) {
        return false;
    }
}
