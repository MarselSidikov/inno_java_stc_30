package ru.inno;

/**
 * 02.12.2020
 * 32. Trees
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface BinarySearchTree<T extends Comparable<T>> {
    void insert(T value);
    void printDfs();
    void printDfsByStack();
    // TODO: выводить по-уровням
    void printBfs();

    // TODO:
    void remove(T value);
    boolean contains(T value);
}
