package ru.inno.custom;

/**
 * 12.11.2020
 * 26. Exceptions
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class IllegalPasswordLengthException extends RuntimeException {
    public IllegalPasswordLengthException(String message) {
        super(message);
    }
}
