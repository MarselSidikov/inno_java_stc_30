package ru.inno.try_catch_throw;

import java.io.IOException;

/**
 * 12.11.2020
 * 26. Exceptions
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainFinally {
    public static void main(String[] args) {
//        try {
//            System.out.println("Hello!");
////            throw new IllegalArgumentException("Bye!");
//            throw new IllegalStateException("BAD!");
//        } catch (IllegalArgumentException e) {
//            try {
//                throw new IOException(e.getMessage());
//            } catch (Exception e1) {
//                System.out.println(e1.getMessage());
//            }
//        } finally { // всегда выполняется
//            System.out.println("Marsel");
//        }
        work();
    }


    public static void work() {
        try {
            System.out.println("in try");
            work();
        } finally {
            System.out.println("in finally");
            work();
        }
    }
}
