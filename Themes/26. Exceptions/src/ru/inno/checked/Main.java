package ru.inno.checked;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * 12.11.2020
 * 26. Exceptions
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        SimpleFileReader reader = new SimpleFileReader();
        String value = reader.readFromFile("input.txt");
        System.out.println(value);
    }
}
