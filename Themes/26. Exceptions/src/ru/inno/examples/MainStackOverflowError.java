package ru.inno.examples;

/**
 * 12.11.2020
 * 26. Exceptions
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainStackOverflowError {
    public static int f(int n) {
//        StackOverflowError
//        if (n == 0) {
//            return 1;
//        }
        System.out.println(n);
        return f(n - 1) * n;
    }
    public static void main(String[] args) {
        System.out.println(f(5));
    }
}
