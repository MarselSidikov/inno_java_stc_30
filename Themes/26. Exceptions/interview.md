# Что выведет программа?

```java
class Program {
public static void main(String[] args){
        try {
            System.out.println("Hello!");
            throw new IllegalArgumentException("Bye!");
        } catch (IllegalArgumentException e) {
            throw new IOException(e.getMessage());
        }
    }
}
```