# Exceptions

* `Exception` - исключительная ситуация, которая возникла во время выполнения программы

* Все исключительные ситуации в Java оборачиваются в объект определенного класса

## Вывод сообщения об исключении

```
Exception in thread "main" java.lang.ArithmeticException: / by zero
	at ru.inno.examples.MainArithmeticException.main(Main.java:17)
```

* `/ by zero` - detail message, более подробное сообщение об ошибке

* `at ru.inno.examples.MainArithmeticException.main(Main.java:17)` - stack trace, место возникновения исключения - класс,метод,файл,строка

* `java.lang.ArithmeticException` - класс исключения, описывающий ошибки, 
связанные с арифметическими операциями, потомок `RuntimeException`.

* `java.lang.NullPointerException` - класс исключения, описывающий ошибки,
связанные с нулевым указателем (объектная переменная не ссылается на реальный объект в памяти),
потомок `RuntimeException`.

* `java.lang.StackOverflowError` - класс исключения, описывающий ошибки,
связанные с переполнением стека вызовов. Потомок `VirtualMachineError`.

* `java.lang.InputMismatchException` - класс исключения, описывающий ошибки, связанные с некорректным форматом 
введенной строки, потомок `NoSuchElementException`

* `java.lang.OutOfMemoryError` - класс исключения, описывающий ошибки, связанные с переполнением памяти,
потомок `VirtualMachineError`.

* `java.lang.FileNotFoundException` - класс, исключения, описывающий ошибки, связанные с "ненахождением файла",
потомок `IOException`

* `IllegalArgumentException` - "ошбика неверного аргумента", потомок `RuntimeException`

* `NoSuchElementException` - "ошибка не соответствия ожиданиям", потомок `RuntimeException`

* `RuntimeException` - "ошибка времени выполнения" (не путать с термином Exception), потомок `Exception`

* `VirtualMachineError` - "ошибка виртуальный машины", потомок `Error`

* `IOException` - "ошибка ввода вывода", потомок `Exception` 

* `Exception` - "предок всех исключений, которые можно как-то исправить", потомок `Throwable`

* `Error` - "предок всех исключений, которые от вас не зависят и вы исправить не можете", потомок `Throwable`

* `Throwable` - верх иерархии исключений

```java
class Throwable {
    private String detailMessage; // сообщение об ошибке
    private StackTraceElement[] stackTrace; // стек вызовов, который привел к исключению
    private Throwable cause = this; // причина ошибки (важно в случае, если причиной этой ошибка ошибки стала другая ошибка)
}

public final class StackTraceElement {
    private String declaringClass; // класс, в котором возникло исключение
    private String methodName; // метод, в котором возникло исключение
    private String fileName; // имя файла
    private int    lineNumber; // номер строки
}
```

### Худшая практика обработки проверяемых исключений

```java
    try {
        fileOpen(fileName);
    } catch (FileNotFoundException e) {
        e.printStackTrace();
    }
```

* Она худшая, потому что программа при наличии исключительной ситуации продолжила свое выполнение. Вы игнорируете исключение.

### Еще одна худшая практика обработки проверяемых исключений

```java
public String readFromFile(String fileName) throws IOException
```

* Она худшая, потому что вы перекладываете ответственность на вызывающий код.

### Лучшая практика

* В месте возникновения проверяемого исключение обернуть его в непроверяемое

```java
try {
        inputStream.read(bytes);
    } catch (IOException e) {
        throw new IllegalStateException(e);
}
```

* В момент возникновения исключения программа прерывается, следовательно, она не будет предпринимать дальнейших действий с уже ошибочным состоянием.

* Используя непроверяемое исключение, вы не засоряете код дальше (throws, try-catch и т.д.)

* Используя обертывания, вы всегда знаете, что там было за исключение изначально.

* Как правило, исправление исключений заключается в том, что "пользователь начинает сначала". Поэтому прерывание "текущего запроса" - единственно верное решение.

* В веб-приложениях (на сайтах) каждый запрос пользователя - это отдельный "поток", и когда происходит ошибка - мы завершаем текущий поток и просим пользователя ввести корректные значения, при этом вся программа не падает, она состоит "потоков".

### ПРИМЕЧАНИЕ

* Проверяемые исключения - атавизм, который разработчики Java добавили, а потом сильно пожалели.