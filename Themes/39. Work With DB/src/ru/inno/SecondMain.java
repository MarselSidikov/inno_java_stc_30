package ru.inno;

import java.sql.*;
import java.util.Scanner;

/**
 * 17.12.2020
 * 39. Work With DB
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class SecondMain {
    private static final String PG_USER = "postgres";
    private static final String PG_PASSWORD = "qwerty007";
    private static final String PG_URL = "jdbc:postgresql://localhost:5432/stc_db_2020";

    public static void main(String[] args) {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(PG_URL, PG_USER, PG_PASSWORD);
            statement = connection.createStatement();
            Scanner scanner = new Scanner(System.in);
            String name = scanner.nextLine();
            String sql = "insert into account(first_name) values('" + name + "')";
            System.out.println(sql);
            statement.executeUpdate(sql);
        } catch (SQLException | ClassNotFoundException throwables) {
            throw new IllegalArgumentException(throwables);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException throwables) {
                    // ignore
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException throwables) {
                    // ignore
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException throwables) {
                    // ignore
                }
            }
        }
    }
}
