package ru.inno;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	    // поток -> stream -> последовательность байт
        // по умолчанию открываются три потока

        // поток ввода по умолчанию System.in,
        // InputStream
        Scanner scanner = new Scanner(System.in);
        // поток вывода по умолчанию System.out
        // PrintStream
        System.out.println("Hello!");
        // поток для вывода ошибок
        // PrintStream
        System.err.println("Error!");
    }
}
