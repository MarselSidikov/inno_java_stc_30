package ru.inno.schema;

import java.io.*;

/**
 * 16.11.2020
 * 27. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) throws Exception {
        InputStream inputStream1 = new FileInputStream("");
        OutputStream outputStream1 = new FileOutputStream("");
        Reader reader1 = new FileReader("");
        Reader reader2 = new InputStreamReader(inputStream1);
        Writer writer1 = new FileWriter("");
        Writer writer2 = new OutputStreamWriter(outputStream1);
    }
}
