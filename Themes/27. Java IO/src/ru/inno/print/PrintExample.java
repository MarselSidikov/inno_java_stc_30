package ru.inno.print;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Writer;

/**
 * 16.11.2020
 * 27. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class PrintExample {
    public static void main(String[] args) throws Exception {
        Writer out;
        PrintWriter writer = new PrintWriter("output_2.txt");
        writer.printf("%4d = %10s \n", 10, "Hello");
        writer.printf("%4d = %10s \n", 5, "Marsel");
        writer.printf("%4d = %10s \n", 8, "Bye");
        writer.printf("%4d = %10s \n", 8, "Java");
        writer.close();

        PrintStream printStream = new PrintStream("output_3.txt");

    }
}
