package ru.inno.streams;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

/**
 * 16.11.2020
 * 27. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class InputStreamReadByLine {

    public static String nextLine(InputStream inputStream) throws Exception {
        byte bytes[] = new byte[100];
        int i = 0;
        int current = inputStream.read();
        while (current != '\r') {
            bytes[i] = (byte)current;
            i++;
            current = inputStream.read();
        }
        return new String(bytes);
    }
    public static void main(String[] args) throws Exception {
        InputStream input = new FileInputStream("input.txt");
        System.out.println(nextLine(input));
        System.out.println(nextLine(input));
        System.out.println(nextLine(input));
    }
}
