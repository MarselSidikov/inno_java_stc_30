package ru.inno.streams;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

/**
 * 16.11.2020
 * 27. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class InputStreamExample {
    public static void main(String[] args) throws Exception {
        // класс, описывающий входной поток
        InputStream inputStream = new FileInputStream("input.txt");

        /*
        int b1 = inputStream.read();
        System.out.println(inputStream.available());
        int b2 = inputStream.read();
        System.out.println(inputStream.available());
        int b3 = inputStream.read();
        System.out.println(inputStream.available());
        int b4 = inputStream.read();
        System.out.println(inputStream.available());
        int b5 = inputStream.read();
        System.out.println(inputStream.available());
        int b6 = inputStream.read();
        System.out.println(inputStream.available());
        System.out.println((char)b1);
        System.out.println((char)b2);
        System.out.println((char)b3);
        System.out.println((char)b4);
        System.out.println((char)b5);
        System.out.println((char)b6);
        */

        // read - возвращает число 0 ... 255 = считанному байту
        // вопрос - почему возвращает int, а не byte?
        // read возвращает -1, когда поток закончился и считывать нечего
        // byte -> -128..127
        // если бы read возвращал byte, то при чтении и возврате -1 было бы не понятно,
        // вы считали байт 255 или поток кончился и вы вернули -1
        int bytesCount = 0;
        int byteFromFile = inputStream.read();
        while (byteFromFile != -1) {
            byteFromFile = inputStream.read();
            System.out.print((char) byteFromFile);
            bytesCount++;
        }
        System.out.println();
        System.out.println(inputStream.read());
        System.out.println(bytesCount);
        inputStream.close();
        System.out.println((byte)240);

        inputStream = new FileInputStream("input.txt");
        byte bytes[] = new byte[inputStream.available()];
        // считает данные из входного потока
        int countOfBytes = inputStream.read(bytes);
        System.out.println("Count of bytes - " + countOfBytes);
        String text = new String(bytes);
        System.out.println(text);
    }
}
