class Program {

	public static int f(int x, int y, int z) {
		System.out.println("--> f(" + x + ", " + y + ", " + z + ")");
		int result = a(x, y, z) - b(x, y, z);
		System.out.println("<-- f = " + result);
		return result;
	}

	public static int a(int x, int y, int z) {
		System.out.println("--> a(" + x + ", " + y + ", " + z + ")");
		int result = d(x, y) * e(y, z);
		System.out.println("<-- a = " + result);
		return result;
	}

	public static int b(int x, int y, int z) {
		System.out.println("--> b(" + x + ", " + y + ", " + z + ")");
		int result = c(x, y) + d(y, z);
		System.out.println("<-- b = " + result);
		return result;
 	}

	public static int c(int x, int y) {
		System.out.println("--> с(" + x + ", " + y + ")");
		int result = x / y;
		System.out.println("<-- c = " + result);
		return result;
	}

	public static int d(int x, int y) {
		System.out.println("--> d(" + x + ", " + y + ")");
		int result = x - y;
		System.out.println("<-- d = " + result);
		return result;
	}

	public static int e(int x, int y) {
		System.out.println("--> e(" + x + ", " + y + ")");
		int result = x + y;
		System.out.println("<-- e = " + result);
		return result;
	}

	public static void main(String[] args) {
		int result = f(16, 20, 15);
		System.out.println(result);
	}
}