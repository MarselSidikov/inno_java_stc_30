package ru.inno;

/**
 * 29.10.2020
 * 21. String
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainConcat {
    public static void main(String[] args) {
        /**
         * ""
         * Marsel
         * Hello
         * Bye
         * s + Marsel
         * s + Hello
         * s + Bye
         */
        String s = ""; // создали объект "" положили в s
        s = s + "Marsel"; // создали объект Marsel, создали объект s + Marsel положили в s
        s = s + "Hello!"; // создали объект Hello, создали объект s + Hello, положили в s
        s = s + "Bye!";
        System.out.println(s);

        String hello = "";
        for (int i = 0; i < 100; i++) {
            hello = hello + i;
        }
        System.out.println(hello);
    }
}
