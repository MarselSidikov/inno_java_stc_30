package ru.inno.stream;

import ru.inno.User;
import ru.inno.UserForReport;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 25.11.2020
 * 29. Stream API
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main3 {
    public static void main(String[] args) {
        User user0 = new User(1L, "User1", "Use1", 44);
        User user1 = new User(2L, "User2", "User2", 30);
        User user2 = new User(3L, "User3", "User3", 25);
        User user3 = new User(4L, "User4", "User4", 48);
        User user4 = new User(5L, "User5", "User5", 10);
        User user5 = new User(6L, "User6", "User6", 15);
        User user6 = new User(7L, "User7", "User7", 35);
        User user7 = new User(8L, "User8", "User8", 44);
        User user8 = new User(9L, "User9", "User9", 21);
        User user9 = new User(10L, "User10", "User10", 40);

        List<User> users = new ArrayList<>();

        users.add(user0);
        users.add(user1);
        users.add(user2);
        users.add(user3);
        users.add(user4);
        users.add(user5);
        users.add(user6);
        users.add(user7);
        users.add(user8);
        users.add(user9);

        int result = users.stream()
                .filter(user -> user.getAge() >= 25)
                .map(User::getAge)
//                .reduce(0, Integer::sum);
                .max(Integer::compareTo).orElse(0);

        users.stream().forEach(user -> System.out.println(user));
        users.stream().forEach(System.out::println);
    }
}
