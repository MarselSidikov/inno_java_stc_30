package ru.inno;

import java.util.Arrays;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        int array[] = new int[20];
        Random random = new Random();
        for (int i = 0; i < 20; i++) {
            array[i] = random.nextInt(100);
        }

        MergeSort.doSort(array);
        System.out.println(Arrays.toString(array));
    }
}
