class Program {

	public static int f(int n) {
		if (n <= 0) {
			System.err.println("IllegalArgumentException");
			return -1;
		}

		System.out.println("--> f(" + n + ")");
		if (n == 1) {
			System.out.println("<-- f = " + 1);
			return 1;
		}
		int previous = f(n - 1);
		int result = previous * n;
		System.out.println("<-- f(" + n + ") = " + previous + " * " + n + " = " + result);
		return result;
	}

	public static void main(String[] args) {
		System.out.println(f(5));	
	}
}