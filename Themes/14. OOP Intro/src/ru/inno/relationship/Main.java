package ru.inno.relationship;


/**
 * 08.10.2020
 * 14. OOP Intro
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {

        Bus bus = new Bus(30);
        Driver driver = new Driver("Марсель", "Сидиков");
        Passenger p1 = new Passenger("Алмаз", "Программист");
        Passenger p2 = new Passenger("Алексей", "Разработчик");
        Passenger p3 = new Passenger("Дмитрий", "Девелопер");
        Passenger p4 = new Passenger("Ildar", "Programmer");

        p1.goToBus(bus);
        p2.goToBus(bus);
        p3.goToBus(bus);
        p4.goToBus(bus);

        driver.goToBus(bus);

        driver.drive();
    }
}
