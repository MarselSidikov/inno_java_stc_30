package ru.inno.relationship;

/**
 * 08.10.2020
 * 14. OOP Intro
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Driver {
    private Bus bus;
    private String firstName;
    private String lastName;

    public Driver(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public void goToBus(Bus bus) {
        this.bus = bus;
        this.bus.takeDriver(this);
    }

    public void drive() {
        this.bus.go();
    }
}
