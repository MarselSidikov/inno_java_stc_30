package ru.inno.procedures;

public class MainProceduresStyle {

    /*
    void work(ru.inno.oop.Human *human) {
        human->isWorker = true;
    }
     */
    public static void work(Human human) {
        human.isWorker = true;
    }

    public static void main(String[] args) {
        Human marsel;
        ru.inno.oop.Human humanFromOop = new ru.inno.oop.Human(10, 10);
//        humanFromOop.isWorker = false;
        marsel = new Human();
        Human denis = marsel;
        work(marsel);
        System.out.println(marsel.isWorker);
        System.out.println(denis.isWorker);
    }
}
