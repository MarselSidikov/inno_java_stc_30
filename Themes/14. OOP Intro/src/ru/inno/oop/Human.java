package ru.inno.oop;

/**
 * 08.10.2020
 * 14. OOP Intro
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */

/**
 * private - модификатор доступа, закрытый (приватный) - уровень доступа, не доступен вне класса
 * public - модификатор доступа, открытый (публичный) - уровень доступа, доступен везде
 * отсутствие модификатора доступа = уровню доступа "пакетный" (package private), член класса доступен
 * везде в рамках одного пакета
 * getAge() - метод доступа типа "геттер"
 * КОНСТРУКТОР - НЕ ЧЛЕН КЛАССА
 */
public class Human {
    // fields, поля, определяют состояние объекта

    // поля (синонимы) -> свойства, характеристики
    private boolean isWorker;
    private double height;
    private int age;

    public Human() {
        this.isWorker = false;
        this.height = 1.70;
        this.age = 0;
        System.out.println("Объект инициализирован");
    }

    public Human(double height) {
        this.height = height;
        this.age = 0;
        System.out.println("Объект инициализирован");
    }

    public Human(double height, int age) {
        this.height = height;
        this.age = age;
        System.out.println("Объект инициализирован");
    }

    // methods, методы, функции и процедуры внутри класса
    // определяют поведение объекта
    public void grow(double value) {
        height += value;
    }

    public void relax() {
        isWorker = false;
    }

    public void work() {
        isWorker = true;
    }

    // метод доступа - геттер
    public int getAge() {
        return this.age;
    }

    // меттод доступа сеттер
    public void setAge(int age) {
        if (age >= 0 && age <= 100) {
            this.age = age;
        } else {
            System.err.println("Недопустимое значение возраста, по умолчанию - 0");
        }
    }

    public boolean isWorker() {
        return isWorker;
    }

    public void setWorker(boolean worker) {
        isWorker = worker;
    }

    public double getHeight() {
        // TODO: сделать проверку
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }
}
