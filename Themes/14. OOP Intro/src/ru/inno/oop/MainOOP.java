package ru.inno.oop;

/**
 * 08.10.2020
 * 14. OOP Intro
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainOOP {
    public static void main(String[] args) {
        Human h = new Human(1.0);
        Human h2 = new Human(1.0);
//        h2.height = 1.95;
//        h.height = 1.85;
        h2.setHeight(1.95);
        h.setHeight(1.85);
        h.work();
        System.out.println(h.isWorker());
        h.grow(0.1);
        h2.grow(0.5);
        System.out.println(h.getHeight());
        System.out.println(h2.getHeight());

        Human human = new Human(1.85);
    }
}
