package ru.inno.excecutors;

/**
 * 07.12.2020
 * 36. ThreadPool
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface TaskExecutorService {
    void submit(Runnable task);
}
