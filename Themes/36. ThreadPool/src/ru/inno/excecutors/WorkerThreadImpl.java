package ru.inno.excecutors;

import java.util.Deque;
import java.util.LinkedList;

/**
 * 07.12.2020
 * 36. ThreadPool
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class WorkerThreadImpl implements TaskExecutorService {

    private final Deque<Runnable> tasks;

    private final WorkerThread workerThread;

    public WorkerThreadImpl() {
        this.tasks = new LinkedList<>();
        this.workerThread = new WorkerThread();
        this.workerThread.start();
    }

    @Override
    public void submit(Runnable task) {
        // если кладем новую задачу в очередь, то мы не хотим, чтобы в этот момент ее меняли
        synchronized (tasks) {
            tasks.add(task);
            // оповестим поток, который был в ожидании о том, что есть новая задача
            tasks.notify();
        }
    }

    private class WorkerThread extends Thread {

        public WorkerThread() {
            super("workerThread");
        }
        @Override
        public void run() {
            Runnable task;
            while (true) {
                // заблокировать очередь задач, чтобы пока мы ее смотрим, никто больше не мог туда ничего положить
                synchronized (tasks) {
                    while (tasks.isEmpty()) {
                        // уходим в ожидание, пока в очередь ничего не положат
                        try {
                            tasks.wait();
                        } catch (InterruptedException e) {
                            throw new IllegalStateException(e);
                        }
                    }
                    // забрали задачу из очереди
                    task = tasks.poll();
                    // начали ее выполнять
                    task.run();
                }
            }
        }
    }
}
