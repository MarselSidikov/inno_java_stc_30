package ru.inno.excecutors;

/**
 * 07.12.2020
 * 36. ThreadPool
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class TaskExecutorThreadPerTaskImpl implements TaskExecutorService {
    @Override
    public void submit(Runnable task) {
        Thread thread = new Thread(task);
        thread.start();
    }
}
