package ru.inno.wait_notify;


import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        Product product = new Product();
        Consumer consumer = new Consumer(product);
        Producer producer = new Producer(product);

        scanner.nextLine();
        consumer.start();
        producer.start();
    }
}
