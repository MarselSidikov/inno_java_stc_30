package ru.inno.wait_notify;

/**
 * 07.12.2020
 * 36. ThreadPool
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Product {
    // true - если продукт можно использовать
    // false - если нельзя использовать
    private boolean isReady;

    // продукт готов, когда его можно использовать
    public boolean isProduced() {
        return isReady;
    }
    // продукт использован, если он не готов
    public boolean isConsumed() {
        return !isReady;
    }

    // готовим - значит приводим в "готовность"
    public void produce() {
        this.isReady = true;
    }
    // потребляем - приводим в "неготовность"
    public void consume() {
        this.isReady = false;
    }
}
