package ru.inno.wait_notify;

/**
 * 07.12.2020
 * 36. ThreadPool
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Producer extends Thread {
    private final Product product;

    public Producer(Product product) {
        this.product = product;
    }

    @Override
    public void run() {
        while (true) {
            // блокируем продукт
            synchronized (product) {
                while (!product.isConsumed()) {
                    System.out.println("Producer: Пока нечего готовить, ждем");
                    try {
                        // если он не потреблен, мы уходим в ожидание
                        product.wait();
                    } catch (InterruptedException e) {
                        throw new IllegalStateException(e);
                    }
                }

                System.out.println("Producer: Подготовили");
                product.produce();
                product.notify();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new IllegalStateException(e);
                }
            }

        }
    }
}
