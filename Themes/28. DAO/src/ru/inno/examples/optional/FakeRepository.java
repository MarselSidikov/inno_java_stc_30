package ru.inno.examples.optional;

import ru.inno.models.User;

import java.util.Optional;

/**
 * 19.11.2020
 * 28. DAO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface FakeRepository {
    Optional<User> findById(Long id);
}
