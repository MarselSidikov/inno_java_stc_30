package ru.inno.examples.optional;

import ru.inno.models.User;

import java.util.Optional;

/**
 * 19.11.2020
 * 28. DAO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class FakeRepositoryImpl implements FakeRepository {
    @Override
    public Optional<User> findById(Long id) {
        if (id.equals(1L)) {
            // у нас есть первый пользователь
            User user = new User(1L, "Марсель", "Сидиков");
            return Optional.of(user);
        }
        // если пользователь не найден - возвращает пустой Optional
        return Optional.empty();
    }
}
