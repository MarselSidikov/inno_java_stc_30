package ru.inno.examples;

import ru.inno.dao.ProductsDao;
import ru.inno.dao.ProductsDaoFileBasedImpl;
import ru.inno.dao.UsersDao;
import ru.inno.dao.UsersDaoFileBasedImpl;
import ru.inno.utils.IdGeneratorFileBasedImpl;

/**
 * 19.11.2020
 * 28. DAO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainFinds {
    public static void main(String[] args) {
        UsersDao userDao =
                new UsersDaoFileBasedImpl("users.txt",
                        new IdGeneratorFileBasedImpl("users_sequence.txt"));

        userDao.findById(1L).ifPresent(user -> {
            System.out.println(user.getFirstName() + " " + user.getLastName());
        });

        System.out.println(userDao.findAll());

        ProductsDao productsDao = new ProductsDaoFileBasedImpl("products.txt");
        System.out.println(productsDao.findAll());
    }
}
