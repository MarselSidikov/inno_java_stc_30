package ru.inno.dao;

import ru.inno.models.Product;
import ru.inno.models.User;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * 19.11.2020
 * 28. DAO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class ProductsDaoFileBasedImpl implements ProductsDao {

    private String fileName;

    private Mapper<String, Product> stringToProductMapper = string -> {
        String data[] = string.split(" ");
        // data[0] -> id
        // data[1] -> name
        // data[2] -> price
        // data[3] -> count
        return new Product(Long.parseLong(data[0]),
                data[1],
                Double.parseDouble(data[2]),
                Integer.parseInt(data[3]));
    };

    public ProductsDaoFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public User findByName(String name) {
        return null;
    }

    @Override
    public Optional<User> findById(Long id) {
        return Optional.empty();
    }

    @Override
    public void save(Product entity) {

    }

    @Override
    public void delete(Product entity) {

    }

    @Override
    public List<Product> findAll() {
        try {
            // создаем пустой список
            List<Product> products = new ArrayList<>();
            // открываем файл для чтения
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            // считываем первую строку
            String current = bufferedReader.readLine();
            // пока строка не null (т.е. есть что считывать!)
            while (current != null) {
                // current = 0 Молоко 50.5 10

                // преобразуем по какому-то правилу строку в объект типа User
                Product product = stringToProductMapper.map(current);
                // добавляем этого пользователя в список
                products.add(product);
                // считываем следующую строку в файле
                current = bufferedReader.readLine();
            }
            // закрываем файл из которого считывали данные
            bufferedReader.close();
            // возвращаем полученный список
            return products;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Product entity) {

    }
}
