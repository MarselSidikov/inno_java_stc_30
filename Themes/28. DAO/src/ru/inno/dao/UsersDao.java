package ru.inno.dao;

import ru.inno.models.User;

/**
 * 16.11.2020
 * 28. DAO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface UsersDao extends CrudDao<User> {
    User findByFirstName(String firstName);
}
