package ru.inno.dao;

import ru.inno.models.User;
import ru.inno.utils.IdGenerator;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * 16.11.2020
 * 28. DAO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// DAO != Repository (раньше)
// DAO == Repository (сейчас)
public class UsersDaoFileBasedImpl implements UsersDao {

    private String fileName;

    private IdGenerator idGenerator;

    private Mapper<User, String> userToStringMapper = user ->
            user.getId() + " " +
                    user.getFirstName() + " " +
                    user.getLastName() + " " +
                    user.getCash() + "\r\n";

    private Mapper<String, User> stringToUserMapper = string -> {
        String data[] = string.split(" ");
        return new User(Long.parseLong(data[0]), data[1], data[2], Double.parseDouble(data[3]));
    };

    public UsersDaoFileBasedImpl(String fileName, IdGenerator idGenerator) {
        this.fileName = fileName;
        this.idGenerator = idGenerator;
    }

    @Override
    public Optional<User> findById(Long id) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();

            while (current != null) {
                String data[] = current.split(" ");

                Long existedId = Long.parseLong(data[0]);

                // если мы нашли такого пользователя
                if (existedId.equals(id)) {
                    User user = stringToUserMapper.map(current);
                    return Optional.of(user);
                }

                current = bufferedReader.readLine();
            }

            bufferedReader.close();
            return Optional.empty();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void save(User user) {
        try {
            user.setId(idGenerator.nextId());
            // открываем поток для записи
            OutputStream outputStream = new FileOutputStream(fileName, true);
            // записали байты
            outputStream.write(userToStringMapper.map(user).getBytes());
            // закрыли поток
            outputStream.close();
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public List<User> findAll() {
        try {
            // создаем пустой список
            List<User> users = new ArrayList<>();
            // открываем файл для чтения
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            // считываем первую строку
            String current = bufferedReader.readLine();
            // пока строка не null (т.е. есть что считывать!)
            while (current != null) {
                // current = 0 Марсель Сидиков

                // преобразуем по какому-то правилу строку в объект типа User
                User user = stringToUserMapper.map(current);
                // добавляем этого пользователя в список
                users.add(user);
                // считываем следующую строку в файле
                current = bufferedReader.readLine();
            }
            // закрываем файл из которого считывали данные
            bufferedReader.close();
            // возвращаем полученный список
            return users;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(User entity) {

    }

    @Override
    public void delete(User user) {

    }

    @Override
    public User findByFirstName(String firstName) {
        return null;
    }
}
