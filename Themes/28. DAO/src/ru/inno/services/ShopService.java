package ru.inno.services;

import ru.inno.models.Product;
import ru.inno.models.User;

/**
 * 19.11.2020
 * 28. DAO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface ShopService {
    void buy(Long userId, Long productId);
}
