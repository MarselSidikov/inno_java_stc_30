package ru.inno;

import java.util.Scanner;

/**
 * 29.10.2020
 * 20. Object
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainEquals {

    public static void equalsThat(Object o1, Object o2) {
        //equals(Object)
        if (o1.equals(o2)) {
            System.out.println("Равны!");
        } else {
            System.out.println("Не равны");
        }
    }
    public static void main(String[] args) {
        Human h1 = new Human("Марсель", "Сидиков", 26,
                1.85);

        Human h2 = new Human("Марсель", "Сидиков",
                26, 1.85);

        System.out.println(h1 == h2);
        System.out.println(h1.equals(h2));

        System.out.println(h1.equals(h2));
        equalsThat(h1, h2);
        h1.equals(new Scanner(System.in));
        h1.equals(null);
    }
}
