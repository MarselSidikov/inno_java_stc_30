package ru.inno;

import java.util.Objects;
import java.util.StringJoiner;

/**
 * 29.10.2020
 * 20. Object
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Human {
    private String firstName;
    private String lastName;
    private int age;
    private double height;

    public Human(String firstName, String lastName, int age, double height) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.height = height;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public double getHeight() {
        return height;
    }

    @Override
    public boolean equals(Object o) {
        // если ссылки совпали - сразу true
        if (this == o) return true;
        // сравниваем классы и null-ссылку, если не совпали - сразу false
        // мы использовали instanсeof, они использовали getClass
        // getClass - быстрее, но мы про него не знаем
        if (o == null || this.getClass() != o.getClass()) return false;
        // явное преобразование и сравнение полей
        Human human = (Human) o;
        return age == human.age &&
                Double.compare(human.height, height) == 0 &&
                Objects.equals(firstName, human.firstName) &&
                Objects.equals(lastName, human.lastName);
    }

    /**
     * Проверяет объекты на эквивалентность
     * h1.equals(h2)
     * @param object ссылка на объект, с которым мы сравниваем наш объект
     * @return возвращает true, если объекты совпадают по тем правилам, которые мы описали
     */
//    @Override
//    public boolean equals(Object object) {
//        // если ссылка на объект, с которым мы сравниваем наш объект нулевая
//        if (object == null) {
//            // сразу говорим, что объекты неэквивалентны
//            return false;
//        }
//        // если ссылка нашего объекта совпадает с ссылкой, которую мы подали на вход
//        // методу
//        if (this == object) {
//            // сразу говорим, что объекты эквивалентны, потому что это один и тот же объект
//            return true;
//        }
//        // поскольку мы передаем объектную переменную класса Object
//        // необходимо убедиться, что их в принципе можно сравнивать
//        // object должен быть экземпляром класса Human
//        if (object instanceof Human) {
//            // нисходящее преобразование, явное преобразование
//            // от более генерализированного класса к более
//            // специализированному
//            Human that = (Human)object;
//            // сравниваем наши поля и поля другого объекта
//            return this.age == that.age &&
//                    this.firstName.equals(that.firstName) &&
//                    this.lastName.equals(that.lastName) &&
//                    this.height == that.height;
//
//        }
//        // если object не является экземпляром нашего класса
//        return false;
//    }

//    public boolean equals(Human that) {
//        return this.age == that.age &&
//                this.firstName.equals(that.firstName) &&
//                this.lastName.equals(that.lastName) &&
//                this.height == that.height;
//    }


//    @Override
//    public String toString() {
//        return this.age + " " + this.firstName + " " + this.lastName + " " + this.height;
//    }


    @Override
    public String toString() {
        return new StringJoiner(", ", Human.class.getSimpleName() + "[", "]")
                .add("firstName='" + firstName + "'")
                .add("lastName='" + lastName + "'")
                .add("age=" + age)
                .add("height=" + height)
                .toString();
    }
}
