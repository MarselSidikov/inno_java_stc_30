package ru.inno;

/**
 * 29.10.2020
 * 20. Object
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainToString {
    public static void main(String[] args) {
        Human h1 = new Human("Марсель", "Сидиков", 26,
                1.85);

        System.out.println(h1);
    }
}
