package ru.inno.intro;

/**
 * 02.11.2020
 * 22. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class iPhone implements Phone {
    public void createPhoto() {
        System.out.println("Сделали фотографию!");
    }
}
