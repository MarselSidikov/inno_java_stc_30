package ru.inno.intro;

/**
 * 02.11.2020
 * 22. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// дженерик ~ обобщенный тип
// T - type, E - entry/entity, K - key, V - value
public class Cover<T> {
    private T phone;

    public void setPhone(T phone) {
        this.phone = phone;
    }

    public T getPhone() {
        return this.phone;
    }
}
