package ru.inno.collections;

/**
 * 28.10.2020
 * 19. Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface Iterator<A> {
    A next();
    boolean hasNext();
}
