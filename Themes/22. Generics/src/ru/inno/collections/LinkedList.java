package ru.inno.collections;

/**
 * 28.10.2020
 * 19. Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class LinkedList<E> implements List<E> {

    private class LinkedListIterator implements Iterator<E> {

        @Override
        public E next() {
            return null;
        }

        @Override
        public boolean hasNext() {
            return false;
        }
    }

    private Node<E> first;
    private Node<E> last;

    private int count;

    private static class Node<F> {
        F value;
        Node<F> next;

        public Node(F value) {
            this.value = value;
        }
    }

    @Override
    public E get(int index) {
        if (index >= 0 && index < count && first != null) {
            int i = 0;
            Node<E> current = this.first;

            while (i < index) {
                current = current.next;
                i++;
            }

            return current.value;
        }
        System.err.println("Такого элемента нет");
        return null;
    }

    @Override
    public int indexOf(E element) {
        int i = 0;
        Node<E> current = this.first;

        while (current != null && current.value != element) {
            current = current.next;
            i++;
        }

        if (current == null) {
            return -1;
        } else {
            return i;
        }
    }

    @Override
    public void removeByIndex(int index) {

    }

    @Override
    public void insert(E element, int index) {

    }

    @Override
    public void add(E element) {
        Node<E> newNode = new Node<>(element);
        if (first == null) {
            first = newNode;
            last = newNode;
        } else {
            // следующий после последнего - новый узел
            last.next = newNode;
            // новый узел теперь последний
            last = newNode;
        }
        count++;
    }

    @Override
    public boolean contains(E element) {
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void removeFirst(E element) {

    }

    // реализация метода получения объекта
    @Override
    public Iterator iterator() {
        return new LinkedListIterator();
    }
}
