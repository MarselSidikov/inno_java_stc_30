package ru.inno.usage;

import java.util.ArrayList;
import java.util.List;

/**
 * 02.11.2020
 * 22. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("Привет");
//        list.add(10);

        String valueFromList1 = list.get(0);
        String valueFromList2 = list.get(1);

        System.out.println(valueFromList1);
        System.out.println(valueFromList2);
    }
}
